require 'dotenv/load'
require 'spec_helper'

describe Recipient do

  let(:username) {ENV['USERNAME']}
  let(:apikey) {ENV['APIKEY']}
  let(:auth) {Authentication.new(username, apikey)}
  let(:token) {auth.generate_token}
  let(:new_user) {Recipient.new(token)}

  describe "#add_recipient" do
      it "should add a new recipient" do
      expect(new_user.add_recipient("Lan")).to include("name" => "Lan")
    end
  end

  describe "#list_recipients" do
    it "should return a list of recipients" do
      expect(new_user.list_recipients[2]).to include("name"=>"Lan")
    end
  end

  describe "#search_recipients" do
    it "should return true if a recipient exists" do
      new_user.list_recipients
      expect(new_user.search_recipients("Lan")).to be_truthy
    end

    it "should return false if a recipient doesn't exists" do
      new_user.list_recipients
      expect(new_user.search_recipients("FakeName")).to be_falsy
    end
  end
end
