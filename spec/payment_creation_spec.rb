require 'dotenv/load'
require 'spec_helper'

describe Recipient do

  let(:username) {ENV['USERNAME']}
  let(:apikey) {ENV['APIKEY']}
  let(:auth) {Authentication.new(username, apikey)}
  let(:token) {auth.generate_token}
  let(:new_user) {Recipient.new(token)}
  let(:payment) {Payment.new(token, new_user)}

  before(:each) do
    new_user.list_recipients
  end

  describe "#send_payment" do
      it "should send a payment to a specific recipient" do
        expect(payment.send_payment(50, "GBP", "Paul")["payment"]).to include("amount"=>"50")
    end
  end

  describe "#list_payments" do
    it "should return a list of payments" do
      expect(payment.list_payments["payments"][8]).to include("amount"=>"50")
    end
  end

  describe "#check_payment" do
    it "should check the status of a payment to Paul" do
      payment.list_payments
      expect(payment.check_payment("Paul")).to include("processing")
    end

    it "should check the status of a payment to Jake" do
      payment.list_payments
      expect(payment.check_payment("Jake McFriend")).to include("paid")
    end
  end
end
