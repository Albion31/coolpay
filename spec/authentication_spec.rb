require 'dotenv/load'
require 'spec_helper'

describe Authentication do

  let(:username) {ENV['USERNAME']}
  let(:apikey) {ENV['APIKEY']}

  describe "#generate_token" do
    it "should generate a token if the authentication details are correct" do
      auth = Authentication.new(username, apikey)
      auth.generate_token
      expect(auth.token).not_to eq('')
    end

    it "should not generate a token with incorrect identification" do
      auth = Authentication.new('Lan', 'fakekey')
      expect{auth.generate_token}.to raise_error(RestClient::NotFound)
    end
  end
end
