[Scenario](#scenario) | [Tech Stack](#tech-stack) | [Setup](#setup) | [Process](#process) | [Reflection](#reflection) | [User Stories](#user-stories) | [Author](#author)

## Scenario

Coolpay is a new company that allows to easily send money to friends through their API.

You work for Fakebook, a successful social network. You’ve been tasked to integrate Coolpay inside Fakebook. A/B tests show that users prefer to receive money than pokes!

You can find Coolpay documentation here: http://docs.coolpayapi.apiary.io/

You will write a small app that uses Coolplay API in Ruby. The app should be able do the following:

- Authenticate to Coolpay API
- Add recipients
- Send them money
- Check whether a payment was successful

## Tech Stack
- Back-End  

  :computer: [Ruby](https://www.ruby-lang.org/en/)  
  :computer: [Coolpay API](https://coolpayapi.docs.apiary.io/)  
  :computer: [Rest-Client](https://github.com/rest-client/rest-client)  
  :computer: [JSON](https://www.json.org/)  
  :computer: [Dotenv](https://github.com/bkeepers/dotenv)  

- Testing  

  :wrench: [Rspec](http://rspec.info/)  
  :wrench: [Pry](https://github.com/pry/pry)

## Setup

1. Clone the repository
```
$ git clone https://Albion31@bitbucket.org/Albion31/coolpay.git
```
2. Install ruby gems
```
$ bundle
```
3. Create a .env file and add your USERNAME and APIKEY

4. Run pry on the command line to run the different functionalities

5. Require the files below:
```
$ require './lib/authentication.rb'
$ require './lib/recipient_creation.rb'
$ require './lib/payment_creation.rb'
```
6. Use pry to run the different functionalities

7. Run test on the command line
```
$ rspec
```

## Process
Since I have never used API's with Ruby or without a framework before, my first step was to learn how to do so by researching it. I have also spent a lot of time trying the Coolpay API to understand how it works and what information can be sent to it and extracted from it.  

I then wrote down my user stories below and diagrammed my model.

Following the SOLID principles, I have decided to create different classes to separate their functionalities to make my code clearer to understand and open for extension.

I started with the Authentication class where a new user is initialised by passing their username and api key. The user can then generate a token by calling the generate_token method on its instance.

Then I created a Recipient class which can be initialised by passing the token generated. The methods available for this class are to add recipients, list them and search by their names.

I have finally created a Payment class which can be initialised by passing a token and user. The user can then send a payment, list all payments and check the payment statuses of a particular recipient.

I have used unit tests with RSpec to TDD my methods which can be found in the spec folder. I have also used pry to feature test my methods.
I have then refactored my methods and tests to follow the DRY principle and moved some methods to private.

## Reflection
If I would have more time, I would have looked into creating a Coolpay class that would handle the above process and I would have added more messages to each methods to confirm whether the method used has been successful or not.
I would also have looked at stubbing my tests and using the mock API for them.

## User Stories

```   
As a developer for Fakebook,
So that the user can use the Coolpay API,
I want the app be able to authenticate the API.
```   

```   
As a developer for Fakebook,
So that the user can send money to the recipients,
I want them to be able add recipients to the app.
```

```  
As a developer for Fakebook,
So that the user can see available recipients,
I want them to be able see the list of recipients.
```

```
As a developer for Fakebook,
So that the user can search for a particular recipient,
I want them to be search recipients by name.
```

```
As a developer for Fakebook,
So that the user can pay the recipient,
I want them to be able to send the money to the correct one.
```

```
As a developer for Fakebook,
So that the user can check if the recipient received the money sent,
I want them to be able check whether a payment has been successful.
```

## Author
[Lan Pham](https://github.com/Albion31)
