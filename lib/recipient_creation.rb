require 'rubygems'
require 'rest-client'
require 'json'

class Recipient

  RECIPIENTS_URL = 'https://coolpay.herokuapp.com/api/recipients'

  def initialize(token)
    @token = token
    @recipients = []
  end

  def add_recipient(recipient_name)
    response = RestClient.post(RECIPIENTS_URL, values(recipient_name), headers)
    JSON.parse(response)['recipient']
  end

  def list_recipients
    response = RestClient.get(RECIPIENTS_URL, headers)
    list = JSON.parse(response)
    @recipients = list["recipients"]
  end

  def search_recipients(name)
    @recipients.each do |recipient|
      if recipient.has_value?(name)
        @recipient = recipient
      end
    end
    @recipient
  end

  private

  def values(recipient_name)
    {
      "recipient": {
        "name": recipient_name
      }
    }.to_json
  end

  def headers
    {
      :content_type => 'application/json',
      :authorization => "Bearer #{@token}"
    }
  end
end
