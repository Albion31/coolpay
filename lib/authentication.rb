require 'rubygems'
require 'rest-client'
require 'json'

class Authentication

  attr_reader :token
  LOGIN_URL = 'https://coolpay.herokuapp.com/api/login'

  def initialize(username, apikey)
    @username = username
    @apikey = apikey
    @token = ''
  end

  def generate_token
    response = RestClient.post(LOGIN_URL, values, headers)
    if response.code == 200
      p "Token generated"
      @token = JSON.parse(response)['token']
    else
      p "There has been an error with your request"
    end
  end

  private

  def values
    {
      "username": @username,
      "apikey": @apikey
    }.to_json
  end

  def headers
    {
      "content-type" => "application/json"
    }
  end
end
