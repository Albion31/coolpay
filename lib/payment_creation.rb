require 'rubygems'
require 'rest-client'
require 'json'

class Payment

  PAYMENT_URL = 'https://coolpay.herokuapp.com/api/payments'

  def initialize(token, user)
    @token = token
    @user = user
  end

  def send_payment(amount, currency, name)
    response = RestClient.post(PAYMENT_URL, values(amount, currency, name), headers)
    JSON.parse(response)
  end

  def list_payments
    response = RestClient.get(PAYMENT_URL, headers)
    @payments = JSON.parse(response)
  end

  def check_payment(name)
    recipient_checked = @user.search_recipients(name)
    name = recipient_checked["id"]
    @payments["payments"].each do |payment|
      if payment.has_value?(name)
        @payment = payment
      end
    end
    @payment["status"]
  end

  private

  def values(amount, currency, name)
    receiving_recipient = @user.search_recipients(name)
    recipient_id = receiving_recipient["id"]

    {
      "payment": {
        "amount": amount,
        "currency": currency,
        "recipient_id": recipient_id
      }
    }.to_json
  end

  def headers
    {
      :content_type => 'application/json',
      :authorization => "Bearer #{@token}"
    }
  end
end
